<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'heysippy');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6 P;>QiUEz7~%v&XsO>9wcsT.tRAy.)i.CTL%8YCpP*`iX%USJP&CN,e-B0X|j|~');
define('SECURE_AUTH_KEY',  'kJ|+W/nwF3zP$3Q]V$H@BXeO9c=.at8oBg#6J`L(~#E6I4L }<xGUC,I_[ >;;Zi');
define('LOGGED_IN_KEY',    ')?*vS/H.mxX-9X-gF?l#]5.k^+^Y6hFknN2rn<W2_A^m;!uBE_/EeYQm#M0T;*0T');
define('NONCE_KEY',        '0_XM#xw<$:k#:zgBR<@()^xz_x={c=dY:om(U/&t0rkdr~ZC0Ni531AXSNq8!vfs');
define('AUTH_SALT',        'jOa#v`,*emy&baVtnd!XG1Q$XFt(Ns$M$lDHQz9ity)Rg0;OSOXp6_eoI+o95aM$');
define('SECURE_AUTH_SALT', 'dM rhCUwIBK *&^2&g|jxv~ni.Kl%C2cm=5vrYmi%mLk?&$y/Pf^=FTP_d;vrDal');
define('LOGGED_IN_SALT',   'bNRwJ`JbP8r#yKTmuP/PaKHwT|!V8mw xKE WHR)Ww*ha294}4SKiS;s|n[2tdbQ');
define('NONCE_SALT',       'O{=6E)]+(<a{/k|_h_,}`%~r5s4u#1C0bSJk%zLG}Y=v%ym=v-Wv1)T{{GrBk;tq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
