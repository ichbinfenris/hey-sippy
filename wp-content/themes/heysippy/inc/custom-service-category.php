<div id="service-carousel">
	<div class="container">
		<div class="row">
                    <h2 class="section-title text-center">Marketplace</h2>
			<div class="service-carousel">
				<?php
				$service_categories = get_terms( 'service_category', array(
                                    'orderby'    => 'term_id',
                                    'hide_empty' => 0
                                ) );
                                ?>
                            <?php foreach($service_categories as $service_cat): ?>
                                <?php if (function_exists('z_taxonomy_image_url')){ ?>
                                <?php $cover_image = z_taxonomy_image_url($service_cat->term_id); ?>
                                <?php } ?>
                                
				<div class="col-md-4 col-sm-4">
					<div class="service service-box text-center">
						<div class="thumbnail">
							<a href="<?php echo get_category_link( $service_cat );  ?>" title="<?php the_title_attribute( 'echo=0' ); ?>" rel="bookmark">
                                                            <img src="<?php echo $cover_image; ?>"/>
							</a>
						</div>
                                            <?php //echo $service_cat->term_id; ?>
						<h2 class="service-title"><a href="<?php echo get_category_link( $service_cat );  ?>" title="<?php the_title_attribute( 'echo=0' ); ?>" rel="bookmark"><?php echo $service_cat->name; ?></a></h2> 
                                                <div class="term-description"><?php echo $service_cat->description; ?></div>
					</div>
				</div>
                            <?php endforeach; ?>
			</div>
		</div>
	</div>


</div>