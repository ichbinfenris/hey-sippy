<?php $contact_page = get_post(15);  ?>
<?php $contact_data = get_field('branches',15); //print_r($contact_data); ?>


<div class="contact-container"> 
    <div class="top-contact">
        <div class="container">
            <h2 class="section-title text-center">Chat To Our Team</h2>
              
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h3>Email Us</h3>
                    <?php echo do_shortcode('[contact-form-7 id="124" title="Contact form 1"]'); ?>
                </div>
              
            </div>
        </div>
        
    </div>
    <div class="bottom-contact">
	<div class="container-fluid">
		<div class="contact-warp">
			<h2 class="section-title text-center">Contact Us</h2>
			<div class="address-boxes" >
				<div class="row">
				<?php foreach($contact_data as $data): ?>
                                    <div class="col-sm-3">
                                    <div class="address-branch text-center">
                                        <h3 class="branch-title"><?php echo $data['branch_title']; ?></h3>
                                        <span class="branch-phone">Phone: <a href="tel:<?php echo $data['branchs_phone']; ?>"><?php echo $data['branchs_phone']; ?></a></span>
                                        <span class="branch-email">Email: <a href="<?php echo $data['branchs_email']; ?>"><?php echo $data['branchs_email']; ?></a></span>
                                        <span class="branch-address">Address:<br/><?php echo $data['branchs_address']; ?></span>
                                    </div>
                                    </div>
				<?php endforeach; ?>
				</div>

			</div>
		</div>
	</div>
    </div>
</div>

<!--/ END Brands Container /--> 
