<!--/ Start CTA Container /-->
<div class="cta-container">
  <div class="container"> 
      <h2 class="section-title text-center">How It Works</h2>
    <div class="custom-cta row">

      <?php

      $args = array(
       'post_type'		 	=> 'cta',
       'orderby' 			=> 'menu_order',
       'order'				=> 'ASC',
       'posts_per_page' 	=> 3
       );
      $the_query = new WP_Query( $args );

      if($the_query->post_count>0){

       $i		= 0;
       $col 	= 3;
       $cta_count = 1;
       while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <div class="col-md-<?php echo  12/$col; ?> col-sm-<?php echo  12/$col; ?>">
            <div  id="cta-<?php the_ID();?>" class="cta text-center">

              
              <h3 class="cta-title"><?php the_title(); ?></h3>
              <div class="cta-summary">
                  <span><?php echo $cta_count; ?>.</span>
                <?php the_content(); ?>

              </div><!-- .entry-summary -->
            </div>
        </div>
      <?php
        $cta_count++;
      endwhile;

    } 
    wp_reset_postdata();
    ?>
    </div>
      <div class="section-footer text-center">
          <a class="view-all" href="marketplace/">Work with us</a>
      </div>
  </div>
</div>
<!--/ ENDCTA Container /--> 

