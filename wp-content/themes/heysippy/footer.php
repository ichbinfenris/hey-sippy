<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */
global $redux_options;
?>

</div><!-- #main .site-main -->

<footer class="site-footer" role="contentinfo">
	<div class="footer-content">
		<div class="container">
			<div id="footer-sidebar" class="row">
				<?php dynamic_sidebar( 'sidebar-footer' ); ?>
			</div>
		</div>
	</div> 

	<!-- .footer-info -->

	<div class="footer-info">

		<div class="container">

			<div class="row">
                            <div class="col-sm-12 text-center">
                                
                            </div>
                            <div class="col-sm-12 text-center">
                                <a id="logo" class="navbar-brand"  href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/hey-sippy-logo.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> logo">
					
				</a>
                                <ul class="socials">
									<li class="social facebook"><a href="<?php echo $redux_options['facebook']; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li class="social twitter"><a href="<?php echo $redux_options['twiter']; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li class="social linkedin"><a href="<?php echo $redux_options['linkedin']; ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li class="social email"><a href="mailto:<?php echo $redux_options['main-email']; ?>" target="_blank"><i class="fa fa-envelope"></i></a></li>
									
								</ul>
                            </div>
				<!--/ Start Copyright Info /-->

				<div class="col-md-6">
					<div class="site-copyright">
						<?php do_action( 'dsa_copyright' ); ?> <a href="tel:<?php echo $redux_options['main-phone']; ?>" target="_blank"><i class="fa fa-phone"></i> <?php echo $redux_options['main-phone']; ?></a> <a href="mailto:<?php echo $redux_options['main-email']; ?>" target="_blank"><i class="fa fa-envelope"></i> EMAIL US</a>
                                                <?php //wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class'=>'list-inline pull-left', 'container' => 'ul','fallback_cb' => '', ) ); ?>
					</div>
				</div>

				<!--/ End Copyright Info /-->
                                <!--
				<div class="col-md-6 ">
					<div class="site-info">
						
						<p><span>Website by</span> <a href="http://www.dsa-global.com" target="_blank">DSA Global</a></p>
					</div>
				</div> -->

			</div> 

		</div><!-- .container -->            

	</div><!-- END .footer-info -->



</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>

</body>
</html>