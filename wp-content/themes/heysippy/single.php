<?php

/**
 * The Template for displaying all single posts.
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */


get_header(); ?>

<div class="container">
	<div class="row">
		
		<div class="col-md-12">
			<div id="content" class="site-content archive-content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>
                                        <?php dsa_content_nav( 'nav-below' ); ?>
					<?php get_template_part( 'content', 'single' ); ?>
					

					<?php
					// If comments are open or we have at least one comment, load up the comment template

					if ( comments_open() || '0' != get_comments_number() )
						//comments_template( '', true );
					?>
				<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div><!-- .col-md-8 -->

		
		
	</div><!-- .row -->
</div>

<?php get_footer(); ?>