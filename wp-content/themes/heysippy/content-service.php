<?php
/**
 * The template used for displaying page content in single-service.php
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */
?>

<?php $feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID),'large'); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('service-box'); ?>>
  <div class="thumbnail" style="background:url(<?php echo $feature_image[0]; ?>)center center no-repeat;">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute( 'echo=0' ); ?>" rel="bookmark">
          <?php
          if ( has_post_thumbnail() ) {
            //the_post_thumbnail('medium');
        }
        else {
            echo '<img alt="file name" src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/thumbnail-default.png" />';
        }
        ?>
    </a>
</div>      
<header class="entry-header">
  <h2 class="entry-title"><?php the_title(); ?></h2>
</header><!-- .entry-header -->



<div class="entry-content">
  <?php the_excerpt(); ?>
</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
