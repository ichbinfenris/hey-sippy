<?php
function dsa_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri() ?>/lib/dsa-login/logo-login.png);
            padding-bottom: 0px;
            margin-bottom:0;
			background-size: auto auto;
			height:100px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'dsa_login_logo' );

function dsa_login_logo_url() {
    return 'http://www.dosa.io/';
}
add_filter( 'login_headerurl', 'dsa_login_logo_url' );

function dsa_login_logo_url_title() {
    return 'DSA Global';
}
add_filter( 'login_headertitle', 'dsa_login_logo_url_title' );

function dsa_login_stylesheet() { ?>
    <link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_template_directory_uri() . '/lib/dsa-login/style-login.css'; ?>" type="text/css" media="all" />
    <?php }
add_action( 'login_enqueue_scripts', 'dsa_login_stylesheet' );