<?php

/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */



get_header(); ?>

<div class="container">

	<div class="row">

		<div class="col-md-12">
			<div id="content" class="site-content" role="main">

				<div class="archive-header page-header has-breadcrumbs">
                                    <div class="col-sm-6">
					<h1 class="entry-title">Marketplace</h1>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="pull-right" id="product-nav-widget">
                                            <span id="nav-control">Select a category</span>
                                            <div id="wrap-dropdown">
                                            <?php dynamic_sidebar( 'sidebar-service' ); ?>
                                            </div>
                                        </div>
                                    </div>
				</div><!-- .entry-header -->

				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>
                                                <?php echo '<div class="col-md-4 col-sm-4">'; ?>
						<?php get_template_part( 'content', 'service' ); ?>
                                                <?php echo '</div>'; ?>
					<?php endwhile; ?>

					<?php pwd_content_nav( 'nav-below' ); ?>

				<?php else : ?>
					<?php get_template_part( 'no-results', 'archive' ); ?>
				<?php endif; ?>

			</div>
		</div>
		
	</div>
</div>


<?php get_footer(); ?>