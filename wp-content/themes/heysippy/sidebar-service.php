<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */
?>
<div id="sidebar-service" class="widget-area" role="complementary">
    <div class="widget">
        <h3 class="widget-title text-center">100% Satisfaction Guaranteed</h3>
        <ul>
            <li>30 day money back guarantee.</li>
            <li>No contracts, cancel anytime.</li>
            <li>Hand-picked, seasoned writers.</li>
        </ul>
    </div>
	<?php //do_action( 'before_sidebar' ); ?>
	<?php //dynamic_sidebar( 'sidebar-service' ); ?>
</div>
<!-- #secondary .widget-area --> 
