<?php
/**
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="thumbnail">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute( 'echo=0' ); ?>" rel="bookmark">
            <?php
              if ( has_post_thumbnail() ) {
                the_post_thumbnail('full');
            }
            else {
                echo '<img alt="file name" src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/thumbnail-default.png" />';
            }
            ?>
        </a>
    </div> 
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
		
	</header><!-- .entry-header -->
	

	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'dsa' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->        
	<footer class="entry-meta">
            <?php dsa_posted_on(); ?>
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'dsa' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'dsa' ) );

			if ( ! true ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( '| %2$s.', 'dsa' );
				} else {
					$meta_text = __( '.', 'dsa' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( '| %1$s | %2$s.', 'dsa' );
				} else {
					$meta_text = __( '| %1$s.', 'dsa' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
		?>

		
	</footer><!-- .entry-meta -->
        
        <div class="sharing">
            <h4>Share This Story, Choose Your Platform!</h4>
            <?php echo do_shortcode('[addtoany]'); ?>
        </div>
</article><!-- #post-<?php the_ID(); ?> -->
