<?php
function register_post_type_service_init() {
  $labels = array(
    'name' 					=> _x( 'Services','post type general name','dsa'),
    'singular_name' 		=> _x( 'Service','post type general name','dsa'),
    'add_new' 				=>  __( 'Add New','dsa'),
    'add_new_item' 			=>  __( 'Add New Service','dsa'),
    'edit_item' 			=>  __( 'Edit Service','dsa'),
    'new_item' 				=>  __( 'New Service','dsa'),
    'all_items' 			=>  __( 'All Services','dsa'),
    'view_item' 			=>  __( 'View Service','dsa'),
    'search_items' 			=>  __( 'Search Services','dsa'),
    'not_found' 			=>  __( 'No services found','dsa'),
    'not_found_in_trash' 	=>  __( 'No services found in Trash', 'dsa'),
    'menu_name' 			=>  __( 'Services','dsa')
  );
  $args = array(
    'labels' 				=> $labels,
    'public' 				=> true,
    'publicly_queryable' 	=> true,
    'show_ui' 				=> true, 
    'show_in_menu' 			=> true, 
    'query_var' 			=> true,
    'rewrite' 				=> array( 'slug' => 'service' ),
    'capability_type' 		=> 'page',
    'has_archive' 			=> true, 
    'hierarchical' 			=> true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-hammer',
    'supports' 				=> array( 'title', 'editor', 'thumbnail', 'excerpt','page-attributes' )
  ); 
  register_post_type( 'service', $args );
  // Register and configure Portfolio Category taxonomy
	$taxonomy_labels = array(
		'name'              => _x( 'Service Categories','taxonomy general name', 'dsa' ),
		'singular_name'     => _x( 'Service Category','taxonomy general name', 'dsa' ),
		'search_items'      =>  __( 'Service Portfolio Categories','dsa' ),
		'all_items'         => __( 'All Service Categories','dsa'),
		'parent_item'       => __( 'Parent Service Categories','dsa'),
		'parent_item_colon' => __( 'Parent Service Category','dsa'),
		'edit_item'         => __( 'Edit Service Category','dsa'),
		'update_item'       => __( 'Update Service Category','dsa'),
		'add_new_item'      => __( 'Add New Service Category','dsa'),
		'new_item_name'     => __( 'New Service Category','dsa'),
		'menu_name'         => __( 'Categories','dsa')
		);
	register_taxonomy( 'service_category', 'service', array(
		'hierarchical'      => true,
		'labels'            => $taxonomy_labels,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'services' )
		)
	);
}
add_action( 'init', 'register_post_type_service_init' );
?>