<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */

get_header(); ?>

<div class="container">
    <div class="row">

        <div class="col-md-9 col-sm-9">

           <div id="content" class="site-content" role="main">

            <?php while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="page-header has-breadcrumbs">
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                        <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
                    </header><!-- .entry-header -->
                    <div class="thumbnail" style="background:url(<?php echo $feature_image[0]; ?>)center center no-repeat;">
                            
                            <?php
                              if ( has_post_thumbnail() ) {
                                the_post_thumbnail('full');
                            }
                            else {
                                echo '<img alt="file name" src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/thumbnail-default.png" />';
                            }
                            ?>
                        
                    </div>
                    <div class="entry-content">
                        <h3 class="text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/description-icon.svg"/><br/>
                            Description</h3>
                        <?php the_content(); ?>
                    </div><!-- .entry-content -->
                    <div class="content-expecting">
                        <h3 class="text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/expect-icon.svg"/><br/>
                            What you can expect:</h3>
                        <?php the_field('what_you_can_expect'); ?>
                    </div>
                    <div class="content-needs">
                        <h3 class="text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/needs-icon.svg"/><br/>
                            What we need from you:</h3>
                        <?php the_field('what_we_need_from_you'); ?>
                    </div>
                    <div class="content-faqs">
                        <h3 class="text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/service-faq-icon.svg"/><br/>
                            FAQs</h3>
                        <?php the_field('faqs'); ?>
                    </div>
                </article><!-- #post-<?php the_ID(); ?> -->
            <?php endwhile; ?>

        </div><!-- #content .site-content -->

    </div><!-- .col-md-8 -->

    <div class="col-md-3 col-sm-3">
        <?php get_sidebar('service'); ?>
    </div><!-- col-md-4 -->

</div><!-- .row -->
</div><!-- .containe -->

<?php get_footer(); ?>