<?php
/**
 * The template used for displaying page content in single-service.php
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */
?>

<?php $feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID),'large'); ?>
<article id="post-<?php the_ID(); ?>" class="service-box">
  <div class="thumbnail" style="background:url(<?php echo $feature_image[0]; ?>)center center no-repeat;">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute( 'echo=0' ); ?>" rel="bookmark">
          <?php
          if ( has_post_thumbnail() ) {
            //the_post_thumbnail('medium');
        }
        else {
            echo '<img alt="file name" src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/thumbnail-default.png" />';
        }
        ?>
    </a>
</div>      
<header class="entry-header">
    <div class="post-header-meta">
        <span><?php the_date(); ?> | </span>
        <span><?php echo get_the_category_list(','); ?></span>
    </div>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute( 'echo=0' ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
</header><!-- .entry-header -->



<div class="entry-content">
  <?php the_excerpt(); ?>
</div><!-- .entry-content -->
<footer>
    <span class="author-meta">By <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author(); ?></a></span>
    <?php //echo do_shortcode('[likebtn]'); ?>
</footer>
</article><!-- #post-<?php the_ID(); ?> -->
