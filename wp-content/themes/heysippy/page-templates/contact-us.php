<?php
/**
 * Template Name: Contact us
 *
 * This is the template that displays Full-width pages.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */
get_header(); ?>


		

			<div id="content-contact" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
				 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                        <div class="top-contact">
                                        <div class="container">
                                            <header class="contact-header text-center">
                                                    <h1 class="entry-title"><?php the_title(); ?></h1>
                                                    <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
                                            </header><!-- .entry-header -->

                                            <div class="entry-content text-center">
                                                    <?php the_content(); ?>
                                                    
                                                    <?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'dsa' ), 'after' => '</div>' ) ); ?>
                                                    
                                                
                                            </div><!-- .entry-content -->
                                            <div class="col-sm-6">
                                                <?php echo do_shortcode('[contact-form-7 id="124" title="Contact form 1"]'); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/Hey-Sippy-Logo-600x600-300x300.png"/>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="bottom-contact">
                                            <?php $contact_data = get_field('branches');  ?>
                                            <div class="container-fluid">
                                                    <div class="contact-warp">
                                                            <h2 class="section-title text-center">Contact Us</h2>
                                                            <div class="address-boxes" >
                                                                    <div class="row">
                                                                    <?php foreach($contact_data as $data): ?>
                                                                        <div class="col-sm-3">
                                                                        <div class="address-branch text-center">
                                                                            <h3 class="branch-title"><?php echo $data['branch_title']; ?></h3>
                                                                            <span class="branch-phone">Phone: <a href="tel:<?php echo $data['branchs_phone']; ?>"><?php echo $data['branchs_phone']; ?></a></span>
                                                                            <span class="branch-email">Email: <a href="<?php echo $data['branchs_email']; ?>"><?php echo $data['branchs_email']; ?></a></span>
                                                                            <span class="branch-address">Address:<br/><?php echo $data['branchs_address']; ?></span>
                                                                        </div>
                                                                        </div>
                                                                    <?php endforeach; ?>
                                                                    </div>

                                                            </div>
                                                    </div>
                                            </div>
                                        </div>

				</article><!-- #post-<?php the_ID(); ?> -->				

			<?php endwhile; // end of the loop. ?>
                        </div>
		


</div>
<?php get_footer(); ?>