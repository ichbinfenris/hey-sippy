<?php

/**
 * Template Name: Strategy Package
 *
 * This is the template that displays Full-width pages.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */


get_header(); ?>

<header class="page-header has-breadcrumbs">
	<h1 class="entry-title text-center"><?php the_title();?></h1>
		
</header><!-- .entry-header -->
<?php $statergy_data = get_field('strategy_packages'); $st_count = 1; ?>
<div class="container text-center">
    <?php foreach($statergy_data as $strategy): ?>
    <div class="col-sm-4 text-center">
        <div class="strategy-box box<?php echo $st_count; ?>">
            <h3 class="package-title"><?php echo $strategy['package_title']; ?></h3>
            <div class="package-desc"><?php echo $strategy['package_descripton']; ?></div>
            <div class="package-price"><?php echo $strategy['package_price']; ?></div>
        </div>
    </div>
    <?php $st_count++; ?>
    <?php endforeach; ?>
    
    <a class="package-link" href="lets-talk">ORDER ONE OF OUR AUDIT PACKAGES NOW</a>
</div>
<div class="container">

	

	<div class="row">
		<div class="col-md-12">
			<div id="content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

			</div>
		</div>
	</div>
</div>
<header class="page-header has-breadcrumbs">
    
	<h1 class="entry-title text-center">Custom Package</h1>
		
</header><!-- .entry-header -->
<?php $custom_package = get_field('custom_package');  ?>
<div class="custom-package-content">
    <div class="container">
        
        <?php echo $custom_package; ?>
    </div>
</div>

<?php get_footer(); ?>