<?php
/**
 * Template Name: Insights
 *
 * This is the template that displays Full-width pages.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package DSA Default Theme
 * @since DSA Default Theme 1.0
 */

get_header(); ?>

<div class="container">
	<div class="row">

	   <div class="col-md-12">
               <?php get_template_part( 'inc/custom', 'blog-slideshow' ); ?>
            </div>

		<div class="col-md-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-2 col-sm-offset-8 col-md-offset-10">
                    <div id="filter-navigation">
                        <span id="filter-label">Filter Categories <i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <div class="button-group filter-button-group">
                            <button data-filter="*">show all</button>
                            <button data-filter=".category-content">Content</button>
                            <button data-filter=".category-design">Design</button>
                            <button data-filter=".category-mobile">Mobile</button>
                            <button data-filter=".category-photography">Photography</button>
                            <button data-filter=".category-social">Social</button>
                            <button data-filter=".category-videography">Videography</button>
                            <button data-filter=".category-website">Websites</button>
                          </div>
                    </div>
                        </div>
                    </div>
			<div id="content" role="main">
			
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>

				<?php 
				
				$args = array(
					'post_type' 		=> 'post',
					'orderby' 			=> 'menu_order',
					'order'				=> 'ASC',
					'posts_per_page' 	=> -1
					);
				
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
					echo '<div class="row">';
					while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<div <?php post_class('col-xs-12 col-sm-4 col-md-4 post-box-grid'); ?>>
					<?php	get_template_part( 'content','post');
						echo '</div>';
					endwhile;
					echo '</div>';
				endif; 
				wp_reset_postdata();
				?>

			</div><!-- #content -->
		</div>
	</div>
</div>

<script type="text/javascript">

  jQuery(document).ready(function($) {
      /**
    var $container = $('#content');

    $container.imagesLoaded( function() {
      $container.masonry({
       // columnWidth: 200,
        itemSelector: '.post-box'
      });
    });
    **/
    // init Isotope
    var $grid = $('#content .row').isotope({
      // options
    });
    // filter items on button click
    $('.filter-button-group').on( 'click', 'button', function() {
      var filterValue = $(this).attr('data-filter');
      $grid.isotope({ filter: filterValue });
    });


  });

</script> 

<?php get_footer(); ?>